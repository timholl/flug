<?php

namespace App;

use App\FlightData\Model\ArrivalComparator;
use App\FlightData\Provider\FlightDataProviderInterface;
use App\FlightData\Provider\HannoverAirportProvider;
use App\FlightData\Store\Repository\StoredArrivalsRepository;
use App\Notification\Notificater;

require __DIR__ . '/vendor/autoload.php';

/** @var FlightDataProviderInterface $arrivalProvider */
$arrivalProvider = HannoverAirportProvider::class;
$storedArrivalsRepository = new StoredArrivalsRepository();

// Get the "current" arrivals
$currentArrivals = $arrivalProvider::getArrivals();

foreach($currentArrivals as $arrival) {
    echo "Checking flight " . $arrival->flightId . "..." . PHP_EOL;

    // Find stored arrival
    $storedArrival = $storedArrivalsRepository->findOneByFlightId($arrival->flightId);

    // Check if is new
    if (null === $storedArrival) {
        echo "Arrival is unknown so far, persisting." . PHP_EOL;
        $storedArrivalsRepository->create($arrival);

        continue;
    }

    // Compare with old
    $comparator = new ArrivalComparator($storedArrival, $arrival);

    if ($comparator->shouldNotify()) {

        echo "Arrival has changed in a way that should lead no notification! Sending notification." . PHP_EOL;

        // Send Notification
        Notificater::send($comparator->getNotificationText());
    } else {
        echo "OK" . PHP_EOL;
    }

    // Update the stored arrival
    $storedArrivalsRepository->update($arrival);
}
