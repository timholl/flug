<?php

namespace App\Notification;

final class Notificater
{
    private const NOTIFICATION_EMAIL = 'root';

    public static function send(string $message): void
    {
        if (false === self::sendMail(self::NOTIFICATION_EMAIL, $message)) {
            throw new \RuntimeException(sprintf('Sending notification to %s failed!', self::NOTIFICATION_EMAIL));
        }
    }

    private static function sendMail(string $recipient, string $message): bool
    {
        return mail(
            $recipient,
            '[Flight] Notification',
            $message
        );
    }
}
