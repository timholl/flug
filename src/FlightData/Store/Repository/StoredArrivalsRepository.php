<?php

namespace App\FlightData\Store\Repository;

use App\FlightData\Model\Arrival;
use RuntimeException;
use SQLite3;

final class StoredArrivalsRepository
{
    private const DB_PATH = 'db.sqlite';

    /*
     * SQLite DB Handle
     */
    private mixed $handle;

    public function __construct()
    {
        // Create DB-File on-demand
        if (!file_exists(self::DB_PATH)) {
            touch(self::DB_PATH);
        }

        // Open DB
        $this->handle = new SQLite3(self::DB_PATH, SQLITE3_OPEN_READWRITE);

        // Create tables on-demand
        if (true !== $this->handle->exec('CREATE TABLE IF NOT EXISTS arrivals (id TEXT PRIMARY KEY, flightNo TEXT NOT NULL, city TEXT NOT NULL, plan TEXT NOT NULL, expected TEXT NOT NULL, gate TEXT NOT NULL, state TEXT NOT NULL)')) {
            throw new RuntimeException('Unable to create tables on-demand.');
        }
    }

    public function findOneByFlightId(string $flightId): ?Arrival
    {
        $statement = $this->handle->prepare('SELECT "id","flightNo", "city", "plan", "expected", "gate", "state" FROM arrivals WHERE id=:id');
        $statement->bindValue(':id', $flightId, SQLITE3_TEXT);

        $result = $statement->execute();

        // No result
        if (false === $array = $result->fetchArray()) {
            return null;
        }

        return new Arrival(
            $array['id'],
            $array['flightNo'],
            $array['city'],
            $array['plan'],
            $array['expected'],
            $array['gate'],
            $array['state'],
        );
    }

    public function create(Arrival $arrival): void
    {
        $statement = $this->handle->prepare('INSERT INTO arrivals ("id", "flightNo", "city", "plan", "expected", "gate", "state") VALUES (:id, :flightNo, :city, :plan, :expected, :gate, :state)');
        $statement->bindValue(':id', $arrival->flightId, SQLITE3_TEXT);
        $statement->bindValue(':flightNo', $arrival->flightNo, SQLITE3_TEXT);
        $statement->bindValue(':city', $arrival->city, SQLITE3_TEXT);
        $statement->bindValue(':plan', $arrival->plan, SQLITE3_TEXT);
        $statement->bindValue(':expected', $arrival->expected, SQLITE3_TEXT);
        $statement->bindValue(':gate', $arrival->gate, SQLITE3_TEXT);
        $statement->bindValue(':state', $arrival->state, SQLITE3_TEXT);

        if (false === $statement->execute()) {
            throw new RuntimeException('Unable to store arrival ' . $arrival->flightId);
        }
    }

    public function delete(Arrival $arrival): void
    {
        $statement = $this->handle->prepare('DELETE FROM arrivals WHERE id=:id');
        $statement->bindValue(':id', $arrival->flightId);

        if (false === $statement->execute()) {
            throw new RuntimeException('Unable to remove arrival ' . $arrival->flightId);
        }
    }

    public function update(Arrival $arrival): void
    {
        // Delete old
        $this->delete($arrival);

        // Insert new
        $this->create($arrival);
    }
}
