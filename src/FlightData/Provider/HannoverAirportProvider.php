<?php

namespace App\FlightData\Provider;

use App\FlightData\Model\Arrival;
use DOMDocument;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use RuntimeException;

final class HannoverAirportProvider implements FlightDataProviderInterface
{
    private const URL = 'https://www.hannover-airport.de/rund-ums-fliegen/ankunft';

    /**
     * @inheritDoc
     */
    public static function getArrivals(): array
    {
        $html = self::query();
        $arrivals = self::parseArrivals($html);

        echo sprintf("Received %d arrivals." . PHP_EOL, count($arrivals));

        return $arrivals;
    }

    private static function query(): string
    {
        $html = file_get_contents(self::URL);

        if (false === $html) {
            throw new RuntimeException(sprintf('Querying %s failed!', self::URL));
        }

        return $html;
    }

    /**
     * @return Arrival[]
     */
    private static function parseArrivals(string $html): array
    {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true); // Hide warnings
        if (false === $doc->loadHTML($html)) {
            throw new RuntimeException('Unable to load the retrieved HTML.');
        }

        $xpath = new DOMXPath($doc);

        /** @var DOMNodeList[] $rows */
        $rows = $xpath->query("//div[@id='completeFlugplanTable']/table/tbody/tr");

        /** @var Arrival[] $arrivals */
        $arrivals = [];

        /** @var DOMElement $row */
        foreach($rows as $row) {

            // Get flight ID (hash, in class attribute)
            $flightId = '';
            foreach(explode(' ', $row->getAttribute('class')) as $class) {
                if (str_starts_with($class, 'flight')) {
                    $flightId = preg_replace('/^flight([a-f0-9]+)$/', '${1}', $class);
                }
            }
            if (empty($flightId)) {
                echo 'Warning: Unable to extract flight id! Skipping.' . PHP_EOL;
                continue;
            }

            /** @var DOMNodeList $columns */
            $columns = $xpath->query('./td', $row);

            $arrivals[] = new Arrival(
                $flightId,
                trim($columns->item(0)->textContent),
                trim($columns->item(1)->textContent),
                trim($columns->item(2)->textContent),
                trim($columns->item(3)->textContent),
                trim($columns->item(4)->textContent),
                trim($columns->item(5)->textContent),
            );
        }

        return $arrivals;
    }
}
