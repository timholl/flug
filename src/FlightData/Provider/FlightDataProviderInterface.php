<?php

namespace App\FlightData\Provider;

use App\FlightData\Model\Arrival;

interface FlightDataProviderInterface
{
    /**
     * @return Arrival[]
     */
    public static function getArrivals(): array;
}
