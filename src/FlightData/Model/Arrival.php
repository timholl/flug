<?php

namespace App\FlightData\Model;

final class Arrival
{
    public function __construct(
        public readonly string $flightId,
        public readonly string $flightNo,
        public readonly string $city,
        public readonly string $plan,
        public readonly string $expected,
        public readonly string $gate,
        public readonly string $state,
    ) {
    }
}
