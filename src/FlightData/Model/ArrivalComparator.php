<?php

namespace App\FlightData\Model;

use LogicException;

final class ArrivalComparator
{
    public function __construct(
        private readonly Arrival $old,
        private readonly Arrival $new,
    ) {
        if ($this->old->flightId !== $this->new->flightId) {
            throw new LogicException('Comparing two different flights!');
        }
    }

    /**
     * Returns true iff a notification should be  sent.
     *
     * Currently, we only notify on gate change.
     */
    public function shouldNotify(): bool
    {
        // Gate changed
        if ($this->old->gate !== $this->new->gate) {
            return true;
        }

        return false;
    }

    public function getNotificationText(): string
    {
        $lines = [];
        $lines[] = sprintf("Flight %s", $this->new->flightId);
        $lines[] = self::getAttributeLine('Flight No', $this->old->flightNo, $this->new->flightNo);

        $lines[] = self::getAttributeLine('Expected Arrival', $this->old->expected, $this->new->expected);
        $lines[] = self::getAttributeLine('Gate', $this->old->gate, $this->new->gate);
        $lines[] = self::getAttributeLine('State', $this->old->state, $this->new->state);

        $lines = array_filter($lines);

        return implode(PHP_EOL, $lines);
    }

    private static function getAttributeLine(string $name, mixed $old, mixed $new, bool $silent = true): ?string
    {
        $ret = sprintf('%s: "%s"', $name, $new);

        if ($old !== $new) {
            $ret .= sprintf(' (changed from "%s")', $old);
        }

        return $ret;
    }
}
