# Flug Watchdog

This command line tool watches the arrivals at [Hannover Airport](https://www.hannover-airport.de) and sends notifications on gate changes.

## Requirements

 * `php-cli` >= 8.1
 * `php-sqlite3`

## Notifications

The script assumes that outgoing mail has been configured, so it can use the default [`mail()`](https://www.php.net/manual/en/function.mail.php) function.
Outgoing notification emails are addressed to `root`; you can redirect them to any external mail you like using an `root: mail@example.com` entry in the `/etc/aliases` file.

## Usage

```shell
$ php index.php
```
